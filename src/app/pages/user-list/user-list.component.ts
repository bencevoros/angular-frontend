import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '@model/user';
import { UserService } from '@service/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  public users: User[];
  public selectedUserId = -1;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.userService.getUsers().subscribe(res => (this.users = res));
  }

  selectUser(id: number) {
    this.selectedUserId = id;
  }

  addUser() {
    this.router.navigateByUrl(`/users/${this.userService.getNextUserId()}`);
  }

  updateUser() {
    this.router.navigateByUrl(`/users/${this.selectedUserId}`);
  }

  deleteUser() {
    this.users = this.userService.deleteUser(this.selectedUserId);
  }
}
