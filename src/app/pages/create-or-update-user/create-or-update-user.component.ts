import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '@model/user';
import { UserService } from '@service/user.service';

@Component({
  selector: 'app-create-or-update-user',
  templateUrl: './create-or-update-user.component.html',
  styleUrls: ['./create-or-update-user.component.scss']
})
export class CreateOrUpdateUserComponent implements OnInit {
  public user: User;
  public title: string;
  public currentYear = new Date().getFullYear();

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const userId = parseInt(params.id, 10);

      let user = this.userService.getUser(userId);
      this.title = 'Felhasználó szerkesztés';

      if (!user) {
        user = this.userService.getEmptyUser();
        this.title = 'Felhasználó létrehozás';
      }

      this.user = user;
    });
  }

  onInputChange(e: KeyboardEvent): void {
    const eventTarget = (e.target as HTMLInputElement);

    this.user[eventTarget.name] = eventTarget.value;
  }

  updateUser(): void {
    this.userService.updateUsers(this.user);

    this.router.navigateByUrl('/users');
  }
}
