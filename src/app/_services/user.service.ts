import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@model/user';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userList: User[];

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    if (!this.userList) {
      this.userList = JSON.parse(localStorage.getItem('userList'));
    }

    return of(this.userList);
  }

  getUser(id: number): User {
    if (!this.userList) {
      this.userList = JSON.parse(localStorage.getItem('userList'));
    }

    return this.userList.find((user) => user.id === id);
  }

  getEmptyUser(): User {
    const user = new User();
    user.name = '';
    user.email = '';
    user.address = '';
    user.birthdate = new Date();
    user.id = this.getNextUserId();

    return user;
  }

  getNextUserId(): number {
    let maxId = 0;

    this.userList.map((user) => {
      if (user.id > maxId) {
        maxId = user.id;
      }
    });

    return maxId += 1;
  }

  updateUsers(updatedUser: User): void {
    let isExist = false;

    const userList = this.userList.map((user) => {
      if (user.id === updatedUser.id) {
        isExist = true;
        return updatedUser;
      }

      return user;
    });

    if (!isExist) {
      userList.push(updatedUser);
    }

    this.userList = userList;

    localStorage.setItem('userList', JSON.stringify(userList));
  }

  deleteUser(id: number): User[] {
    const userList = this.userList.filter((user) => user.id !== id);
    this.userList = userList;

    localStorage.setItem('userList', JSON.stringify(userList));
    return userList;
  }
}
